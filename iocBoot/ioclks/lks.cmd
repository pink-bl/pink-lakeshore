#!../../bin/linux-x86_64/lks

## You may have to change lks to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/lks.dbd"
lks_registerRecordDeviceDriver pdbbase

#Define protocol path
epicsEnvSet("STREAM_PROTOCOL_PATH", ".")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "70000")

## connect to device
drvAsynIPPortConfigure ("L0","172.17.10.28:7777",0,0,0)
#drvAsynIPPortConfigure ("L0","127.0.0.1:50123",0,0,0)

## Load record instances
dbLoadTemplate("${TOP}/iocBoot/${IOC}/lks.substitutions")
dbLoadRecords("db/aliases.db","BL=PINK,DEV=LKS")

cd "${TOP}/iocBoot/${IOC}"

## Autosave Settings
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

## Autosave Settings
create_monitor_set("auto_settings.req", 30, "P=PINK:LKS")

## Start any sequence programs
#seq sncxxx,"user=epics"
